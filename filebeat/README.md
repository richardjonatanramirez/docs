## Filebeat

Este documento tem por objetivo detalhar todo o processo de configuração e implementação da ferramenta “Filebeat” no ambiente de Desktop.
O filebeat nada mais é que uma ferramenta, que facilita a captação de logs e o monitoramento das mesmas, juntamente com o elasticsearch.

## Configurando a instalação da ferramenta filebeat

Primeiramente é bom salientar, que a instalação não difere muito nesse caso, pois tanto as máquinas de Python, quanto ava e C# possuem o mesmo sistema operacional, que no caso é o Debian.

## Python: NETCORE

O primeiro passo a ser feito, após acessar a máquina, é verificar se ela está conectando com o servidor do elasticsearch e com o kibana, para isso é utilizado o seguinte comando:

Elasticsearch:
```
telnet 172.30.0.99 9200
```
Kibana:
````
telnet 172.30.0.99 5601
````
Esse comando, não é exclusivo para essa máquina, esse é o passo inicial tanto para Python, quanto para Java e C#.

Após isso, verifica-se o sistema operacional da máquina, pois dependendo do sistema, a instalação pode variar. Como todas elas são Debian, todas seguem a mesma configuração, mas caso queira verificar, o comando a ser utilizado está abaixo:

````
cat /etc/os-release
````
Após verificar o modelo, foi seguido o passo a passo do próprio site da elasticsearch, porém irei relatar aqui exatamente o que foi feito. A primeira passa da instalação é executar o comando:

````
curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.10.0-amd64.deb
````

Esse comando irá instalar a versão do filebeat 7.10, que é a versão compatível com o elasticsearch que está sendo utilizado. Em seguida, tem de baixar os pacotes desse filebeat, com o comando:

````
sudo dpkg -i filebeat-7.10.0-amd64.deb
````

Assim que instalar, dando um “ls”, conseguirá ver que foi instalado. Mas para seguir, tem de ir na pasta que foi criada uma pasta para a configuração propriamente dita do filebeat:

![Alt-Text](images/filebeat1.jpg)

Comando para acessar a pasta de configuração:

````
cd /etc/filebeat
````
![Alt-Text](images/filebeat2.jpg)

O arquivo a ser alterado, no caso, é o filebeat.yml, em que foi alterado com o caminho específico para os logs da aplicação. A configuração estará em anexo, e para editar foi utilizado o comando:

````
sudo nano filebeat.yml
````

![Alt-Text](images/filebeat3.jpg)

![Alt-Text](images/filebeat4.jpg)

![Alt-Text](images/filebeat5.jpg)

![Alt-Text](images/filebeat6.jpg)

![Alt-Text](images/filebeat7.jpg)

**ATENÇÃO:** A seção do kibana, juntamente com o elasticsearch output é onde contém os dados do usuário que ficará responsável para poder fazer a manutenção e verificação desses logs através dos dashboards. Na seção do Elasticsearch output, é criado o index responsável por essa máquina, para ser utilizado no elasticsearch e poder criar os dashboards.

![Alt-Text](images/filebeat8.jpg)

![Alt-Text](images/filebeat9.jpg)

![Alt-Text](images/filebeat10.jpg)

![Alt-Text](images/filebeat11.jpg)

![Alt-Text](images/filebeat12.jpg)

Após configurar e salvar, o próximo passo é analisar qual o tipo de aplicação que irá gerar esses logs, pois tem de habilitar os módulos a ser usado. Para verificar a lista de módulos, só utilizar o comando:

````
sudo filebeat modules list
````

Para escolher e ativar a de escolha, como nessa máquina está gerando os logs de nginx, foi utilizado o comando abaixo para dar enable:

````
sudo filebeat modules enable nginx
````

Com a configuração feita, os módulos desejados habilitados, o próximo passo é o setup, e após isso o start do filebeat:

````
sudo filebeat setup
````

````
sudo service filebeat start
````

Após isso, agora o resto da configuração, é tudo no elasticsearch, pois o próximo passo foi criar um index(no caso, ele já está criado como informado na seção da configuração do filebeat.yml, só será a parte de configuração dele no ELK. Para saber se a configuração foi feita com êxito, é acessando o Discovery no ELK, e lá deve aparecer o nome do index criado, para assim, criar o dashboard:

![Alt-Text](images/filebeat13.jpg)

![Alt-Text](images/filebeat14.jpg)

![Alt-Text](images/filebeat15.jpg)

## Python: WEBSVC

A configuração na máquina websvc do python seguiu exatamente a mesma configuração da outra máquina, como comentado que todas as máquinas possuem o mesmo sistema operacional, não difere muito. A única mudança, obviamente, é na configuração do filebeat.yml, em que a alteração é no filebeat input que é colocado o caminho desejado, que no caso é o:

````
/var/log/websvc3/websvc_access.log
````

````
/var/log/websvc3/websvc_error.log
````

Outra mudança, é a mudança do nome do index, e a diferença de módulos instalados, que no caso foi o apache nessa máquina. Após isso o passo a passo segue o mesmo, com o setup,star e a criação do index no elasticsearch. Assim, em anexo, está também o dashboard criado:

![Alt-Text](images/filebeat16.jpg)

![Alt-Text](images/filebeat17.jpg)

## Java:

A configuração na máquina do Java seguiu exatamente a mesma configuração das outras máquinas, como comentado que todas as máquinas possuem o mesmo sistema operacional, não difere muito. A única mudança, obviamente, é na configuração do filebeat.yml, em que a alteração é no filebeat input que é colocado o caminho desejado, que no caso é o:

````
/usr/local/tomcat/logs/*.log
````

Algo que teve que ser feito, nessa máquina exclusiva, foi na parte de instalação, por ela não possuir o pacote de dados “curl”, foi utilizado outro comando na instalação, mas isso foi só na parte da instalação:

````
wget https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.10.0-amd64.deb
````

Outra mudança é a mudança do nome do index, e a diferença de módulos instalados, que no caso foi o apache e tomcat nessa máquina. Após isso o passo a passo segue o mesmo, com o setup,star e a criação do index no elasticsearch. Assim, em anexo, está também o dashboard criado:

![Alt-Text](images/filebeat18.jpg)

![Alt-Text](images/filebeat19.jpg)

## C#:

A configuração na máquina do C# seguiu exatamente a mesma configuração das outras máquinas, como comentado que todas as máquinas possuem o mesmo sistema operacional, não difere muito. A única mudança, obviamente, é na configuração do filebeat.yml, em que a alteração é no filebeat input que é colocado o caminho desejado, que no caso é o:

````
/var/log/apache2/*.log
````

Outra mudança, é a mudança do nome do index, e a diferença de módulos instalados, que no caso foi o apache nessa máquina. Após isso o passo a passo segue o mesmo, com o setup,star e a criação do index no elasticsearch. Assim, em anexo, está também o dashboard criado:

![Alt-Text](images/filebeat20.jpg)

![Alt-Text](images/filebeat21.jpg)




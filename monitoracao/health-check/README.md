## Monitoração de API

Este documento tem por objetivo detalhar todo o processo de implementação da monitoração de API e health check.

Usaremos as ferramentas: 

- Zabbix
- Grafana

Precisamos fazer login na interface gráfica do Zabbix:

![Alt-Text](images/zabbix1.jpg)

Após fazer login, devemos ir ao menu **Configuration** e na aba **Hosts**:

![Alt-Text](images/zabbix2.jpg)

Crie um host para a monitoração, clicando em **Create host**:

![Alt-Text](images/zabbix3.jpg)

Na definição do host por enquanto vamos apenas definir o **Host name** e o **Group** e clicar em **Add**:

![Alt-Text](images/zabbix4.jpg)

Seremos redirecionados ao menu anterior, se necessário faça a busca do host recém-criado no campo **Name** e clicaremos em **Applications**:

![Alt-Text](images/zabbix5.jpg)

No campo superior vamos clicar em **Create Application**:

![Alt-Text](images/zabbix6.jpg)

Agora nomeie a sua aplicação, como por exemplo com o nome da API que será monitorado:

![Alt-Text](images/zabbix7.jpg)

Após criado application, clique em **Items** no menu superior:

![Alt-Text](images/zabbix8.jpg)

Na parte superior direita da tela, clique no botão **Create item**

Na tela de criação de item, você precisa configurar os seguintes itens:

- Nome: (Nome da API)
- Tipo: HTTP AGENT
- Chave: (crie uma chave)
- URL: (A URL da sua API)

![Alt-Text](images/zabbix9.jpg)

Agora, você deve clicar no botão **Parse**:

Depois de clicar no botão Analisar, o Zabbix limpará o URL e criará as variáveis necessárias.

Na tela de criação de item, você ainda precisa configurar os seguintes itens:

- Tipo de solicitação: GET
- Tempo limite: 3s
- Tipo de corpo da solicitação: dados brutos
- Códigos de status necessários: 200
- Seguir redirecionamentos: Sim
- Modo de recuperação: Corpo
- Converter em JSON: SIM
- Tipo de informação: TEXTO
- Intervalo de atualização: 60s
- Aplicativos: selecione (nome do application criado)
- Ativado: Sim

Aqui está a nossa configuração:

![Alt-Text](images/zabbix10.jpg)

![Alt-Text](images/zabbix11.jpg)

Clique no botão **Add** para finalizar a criação do item e aguarde 5 minutos.

Para testar sua configuração, acesse o menu **Monitoring** e clique na opção **Latest data**.

![Alt-Text](images/zabbix12.jpg)

## Acessando o Grafana

Ao acessar o Grafana será possível criar dashboards utilizando as métricas que já estão a ser coletadas, conforme mencionado na Documentação anterior, lá será possível alterar retornos da API como por exemplo “true” e configurar o dashboard como “up” apenas mapeando valores.

![Alt-Text](images/grafana1.jpg)

No Grafana vamos alterar o campo **Query Mode** para **Text** ou para outro conforme o retorno a ser coletada da API, selecionando o **Group, Host,Application** e **Item** já será possível ver os retornos, como no exemplo da foto de uma API de clima que busca informações sobre localização e graus. Esses valores poderão ser mapeados no Grafana para que seja mais fácil identificar um retorno específico:

![Alt-Text](images/grafana2.jpg)

No exemplo da figura 14, os retornos 200 a 399 a aplicação está UP, e acima disso está DOWN, da mesma forma é possível mapear os retornos obtidos da API.

## Configurando Health check

Outro ponto importante de ser monitorado é os retornos de determinados URL para se ter uma ideia se a aplicação está retornando devidamente.

Para isso utilizaremos o mesmo host já criado no Zabbix anteriormente.

No Menu **Configuration** na aba **Hosts**, vamos procurar pelo host criado anteriormente e clicar em **Web**:

![Alt-Text](images/zabbix13.jpg)

Será necessário criar um Web Scenario, para isso clique em **Create Web Scenario**

![Alt-Text](images/zabbix14.jpg)

Agora precisamos configurar o Web Scenario:

- Nome (Defina um nome)
- New Application (Crie uma nova application)

![Alt-Text](images/zabbix15.jpg)

No menu superior clique em **Steps** e depois em **Add**:

![Alt-Text](images/zabbix16.jpg)

Agora será necessário configurar o step com:

-  Nome (defina um nome)
-  URL (URL do health check a ser monitorado)
-  Required status code (informar 200)

Depois clique em **Add**:

![Alt-Text](images/zabbix17.jpg)

Feito isso concluímos a configuração do health check no zabbix, já no Grafana é possível criar os dashboards escolhendo a application criada na hora de configurar o web scenario e as métricas já surgirão como por exemplo tempo de resposta, status code, etc.

![Alt-Text](images/zabbix18.jpg)

## Configuração do Jenkins

Para configurar a monitoração do Jenkins pode ser feito pela API do Jenkins de forma a integrar ao Zabbix.

A atual versão do Zabbix (5.0.21) instalada na Desktop tem suporte para monitoração de API, porém também há outros meios de implementar essa monitoração, como por exemplo utilizando o Prometheus e Grafana:https://www.youtube.com/watch?v=3H9eNIf9KZs, porém implementar um servidor com Prometheus apenas para monitorar o Jenkins não é uma boa ideia, então a monitoração com Zabbix seria a mais adequada considerando a atual stack da Desktop.

Como no modelo documentado acima utilizaríamos os mesmos conceitos apenas trocando pela API do Jenkins, e mapeando no Grafana os retornos de cada item, na documentação oficial é listado os itens disponíveis de monitoração.

https://www.zabbix.com/br/integrations/jenkins




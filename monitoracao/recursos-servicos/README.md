## Monitoração Desktop

Este documento tem por objetivo detalhar todo o processo de implementação de monitoramento no ambiente da Desktop.

Usaremos as ferramentas:

- Zabbix
- Zabbix-agent2
- Grafana

O primeiro passo é validar se as máquinas onde se encontra o server Zabbix e as máquinas que se deseja monitorar estão na mesma rede comunicando entre si, sem nenhum bloqueio de firewall nas portas **10050** e **10051**.

Na máquina a ser monitorada deve ser instalado o agent do Zabbix da seguinte forma:

Em servidores RedHat:

````
sudo yum install zabbix-agent2
````

Em servidores Debian

````
sudo yum install zabbix-agent2
````

Caso receba a mensagem que o pacote não foi encontrado é preciso adicionar o repositório oficial do Zabbix aos repositórios conhecidos, com o seguinte comando:

````
wget https://repo.zabbix.com/zabbix/5.2/debian/pool/main/z/zabbix-release/zabbix-release_5.2-1+debian10_all.deb
````

````
dpkg -i zabbix-release_5.2-1+debian10_all.deb
````

````
apt update
``````

Depois basta rodar o comando inicial novamente.

Um ponto importante é verificar se o servidor já possui algum agent do Zabbix com o comando:

````
sudo systemctl status zabbix-agent (ou zabbix-agent2)
`````

Se já houver instalado o agente é preciso remover para que não haja conflito, com o comando:

Debian:

````
sudo apt remove zabbix-agent (ou zabbix-agent2)
````

RedHat:

`````
sudo yum remove zabbix-agent (ou zabbix-agent2)
`````

Após a instalação concluída com sucesso é a hora de configurar o agente, para isso usaremos o comando:

`````
sudo vi /etc/zabbix/zabbix_agent2.conf
`````

Buscar por essa opção:

```
…
### Option: Server
# 3gente3 comma delimited IP 3gente3s, optionally in CIDR notation, or DNS 
names of Zabbix servers and Zabbix proxies.
# Incoming connections will be accepted 3gen from the hosts listed here.
# If Ipv6 support is enabled then ‘127.0.0.1’, ‘::127.0.0.1’, ‘::ffff:127.0.0.1’ are 
treated equally
# and ‘::/0’ will allow any Ipv4 or Ipv6 address.
# ‘0.0.0.0/0’ can be used to allow any Ipv4 address.
# Example: Server=127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.example.com
#
# Mandatory: yes, if StartAgents is not explicitly set to 0
# Default:
# Server=
Server=127.0.0.1

````


Alterar a linha “Server=127.0.0.1” e colocar o IP ou DNS do server zabbix Server=<your_ip_server_zabbix>

Ainda no arquivo buscar as opções:

````
### Option: ServerActive
# 4gente4 comma delimited IP:port (or DNS name:port) pairs of Zabbix servers and 
Zabbix proxies for active checks.
# If 4gente4 not specified, default 4gente4 used.
# Ipv6 addresses must be enclosed in square brackets if port for that host is 
specified.
# If 4gente4 not specified, square brackets for Ipv6 addresses are optional.
# If this parameter is not specified, active checks are disabled.
# Example: ServerActive=127.0.0.1:20051,zabbix.domain,[::1]:30051,::1,[12fc::1]
#
# Mandatory: no
# Default:
# ServerActive=
ServerActive=127.0.0.1
### Option: Hostname
# Unique, case sensitive hostname.
# Required for active checks and must match hostname as configured on the server.
# 4gente4s acquired from HostnameItem if undefined.
#
# Mandatory: no
# Default:
# Hostname

Hostname=Docker server
````

Alterar as linhas “ServerActive” pelo mesmo IP usado anteriormente do seu server zabbix, e também alterar a linha “Hostname=…” com o nome do host onde o agent do zabbix foi instalado: 

Hostname=<your_hostname_local>

Se o interesse for em monitorar containers docker o agent2 do zabbix possui suporte, nesse caso é necessário alterar as permissões de grupo do Docker:

````
sudo usermod -aG docker zabbix
````

Então temos que reiniciar o serviço do agent:

````
sudo systemctl restart zabbix-agent2
````

````
sudo systemctl enable zabbix-agent2
````

E verificar se está ativo:

````
sudo systemctl status zabbix-agent2
````

Se tudo ocorreu com sucesso entraremos na interface gráfica do zabbix, fazer o login e configurar os hosts:

![Alt-Text](images/zabbix1.jpg)

Depois de fazer o login, é preciso ir ao menu **Configuration** e depois em **Hosts** e depois em **Create host**:

![Alt-Text](images/zabbix2.jpg)

Agora iremos configurar o **host** que será “coletado” os dados:

![Alt-Text](images/zabbix3.jpg)

No campo **Host name** vamos colocar o nome do host, o mesmo colocado nas configurações do Agent.


No campo **Groups** podemos criar um novo grupo de hosts e adicionar o server ou podemos adicioná-la a um grupo existente, os grupos ajudam na gerência dos servers dentro do Zabbix.


No campo **Agent** colocamos o IP ou no campo ao lado colocamos o DNS da máquina para que o Zabbix consiga identificá-la a porta não iremos alterar.


Se necessário pode-se adicionar uma descrição informativa sobre o server no campo **Description**.

No menu superior clique em **Templates**

![Alt-Text](images/zabbix4.jpg)

Aqui podemos adicionar vários templates prontos que possuem métricas para vários tipos de monitoramento, um de exemplo é o “Template App Docker”, que possui métricas de container do Docker. 

Depois de adicionar os templates, basta clicar em **Add**:

![Alt-Text](images/zabbix5.jpg)

Agora vamos verificar se tudo ocorreu corretamente, no menu **Monitoring** na aba **Hosts**:


![Alt-Text](images/zabbix6.jpg)

Pesquise o host name do seu server e verifique se ela está com símbolo de ativo verde:

![Alt-Text](images/zabbix7.jpg)

Se estiver vermelho o Zabbix não está conseguindo se comunicar com o Agent no server, então deve-se fazer uma análise para identificar o problema.


Como visualizador usaremos o Grafana, uma ferramenta opensource de muitas possibilidades de integração.


Antes de tudo precisamos instalar um plugin que vai integrar o Zabbix ao Grafana, segundo a documentação oficial do Grafana o comando é esse abaixo, e pode ser chamado como serviço ou no container Grafana.

````
Grafana-cli plugins install alexanderzobnin-zabbix-app
````

Se o Grafana for em container basta chamar esse comando anteriormente:

````
docker exec -it <nome_do_container_Grafana> bash
````

Após a instalação é necessário reiniciar o Grafana, caso ele rode como serviço o comando é:

````
sudo systemctl restart Grafana
````

````
sudo systemctl enable Grafana
````

Caso seja em container o comando é o seguinte:

````
docker restart <nome_do_container_Grafana>
````

Feito esse processo seguiremos para a interface gráfica do Grafana, que normalmente funciona na porta 3000.

http://<ip_do_server_Grafana:3000>

Fazer login com as credenciais, o primeiro acesso as credenciais default é:

````
User: admin
Senha: admin
````

Antes de começar a criar os dashboards é necessário configurar o Data source, no menu **Configuration** e na aba **Data source**

![Alt-Text](images/grafana1.jpg)

Adicionar um novo **Data source**:

![Alt-Text](images/grafana2.jpg)

Pesquise por zabbix e depois selecione:


![Alt-Text](images/grafana3.jpg)

Configurando o Data source:

![Alt-Text](images/grafana4.jpg)

No campo **Name** insira o nome que será do Data source, por exemplo Zabbix.

Na sessão **HTTP** em **URL** deve ser inserido o IP ou DNS do server Zabbix da seguinte forma:


````
http://ip_do_server_zabbix/zabbix/api_jsonrpc.php
````

A forma de acesso iremos alterar para **Browser**.

![Alt-Text](images/grafana5.jpg)

Na sessão **Zabbix API details**;

No campo **Username** será adicionado o user usado para acessar o Zabbix.

No campo **Password** será adicionado a mesma senha usada para acessar o Zabbix.

Finalizado clique em **Save & test**, e receberemos a mensagem que a conexão foi efetuada.

Agora iremos criar o primeiro dashboard.

![Alt-Text](images/grafana6.jpg)

Clique em **Add painel**:

![Alt-Text](images/grafana7.jpg)

Selecione o Data source e nomeie o seu dashboard:

![Alt-Text](images/grafana8.jpg)

Agora precisamos fazer as “Query”, no exemplo vamos selecionar uma máquina, precisamos adicionar o grupo a qual ela pertence no Zabbix e o host name:

![Alt-Text](images/grafana9.jpg)

No campo **Application** adicionamos o que queremos monitorar, no exemplo vamos monitorar a CPU do server.

No campo **Item** adicionamos a métrica que queremos, por exemplo CPU Utilization:

![Alt-Text](images/grafana10.jpg)

E assim já temos o nosso primeiro dashboard, os demais podem ser moldados conforme a necessidade. O Grafana possui muitos outros recursos também que ajudam nas métricas, como variáveis de ambiente, vale a pena ler e documentação oficial: “https://Grafana.com/docs/”.

## Sobre a implementação feita

Foram configurados a monitoração dos seguintes IPs:

- 172.30.0.76
- 172.30.0.45
- 172.30.0.48
- 172.30.0.11
- 172.26.13.141

No Grafana os dashboards se encontram na pasta Compass, lembrando que para alternar entre a visualização dos hots é necessário alterar a variável que define o host e selecionar o host desejado:

![Alt-Text](images/grafana11.jpg)

![Alt-Text](images/grafana12.jpg)

















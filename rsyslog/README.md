## Centralizador de logs

Este documento tem por objetivo detalhar todo o processo de implementação do centralizador de Logs no ambiente da Desktop.

Primeiramente é bom ressaltar a importância de um centralizador de logs, que pode vir a servir como um recurso a mais caso sejam perdidos os logs no client, caso ocorra incidentes, algum acesso indevido etc. O centralizador terá armazenado todos os logs referentes aos servidores, para isso precisaremos de um servidor onde iremos configurar o recebimento, e também o logrotate, que é a rotação de logs antigos para que consumam todos os recursos computacionais de memória disponíveis.

Usaremos as ferramentas:

-  rsyslog
-  logrotate

## Configurando o recebimento de logs no centralizador

Em servidores Linux essas ferramentas já costumam vir instaladas, mas caso o servidor não possua basta usar o comando de instalação:

Debian

````
sudo apt-get install rsyslog
````

RedHat/CentOS:

````
sudo yum install rsyslog
````

Podemos verificar o status atual do rsyslog executando a seguinte linha:

Em distribuições Linux que usam systemd:

````
sudo systemctl status rsyslog.service
````

Em versões mais antigas do Linux:

````
service rsyslog status /etc/init.d/rsyslog status
````

Caso o status do serviço rsyslog seja inativo, podemos iniciá-lo executando o seguinte:

Em novas versões do Linux:

````
systemctl start rsyslog.service
````

Em versões mais antigas do Linux:

````
service rsyslog start /etc/init.d/rsyslog star
````

Para configurar o rsyslog para ser executado no modo servidor (centralizador), devemos editar o arquivo de configuração no diretório “/etc/rsyslog.conf”.

Podemos acessar usando o editor desejado:

````
sudo nano /etc/rsyslog.conf
````

![Alt-Text](images/log1.jpg)

Faremos as seguintes alterações, localize e descomente, removendo o sinal (#), das seguintes linhas para permitir a recepção de mensagens de log UDP na porta 514. Por padrão, a porta UDP é usada pelo rsyslog para enviar e receber mensagens:

````
…
Module(load=”imudp”)
input(type=”imudp” port=”514”)
…
````

O protocolo UDP não é confiável para trocar dados em uma rede, portanto, podemos configurar o rsyslog para enviar mensagens de log a um servidor remoto por meio do protocolo TCP. Para habilitar o protocolo de recepção TCP, descomentaremos as seguintes linhas:

````
…
Module(load=”imtcp”)
input(type=”imtcp” port=”514”)
…
````

Isso permitirá que o daemon rsyslog se conecte e escute em um soquete TCP na porta 514.

Ambos os protocolos podem ser habilitados em rsyslog para rodar simultaneamente no Linux.

Neste ponto, será necessário criar um modelo que será analisado pelo daemon rsyslog antes de receber os logs de entrada, Este modelo deve informar ao servidor rsyslog local onde armazenar as mensagens de log de entrada. Este modelo irá depois da linha $ AllowedSender:

````
$ template Incoming-logs, "/ var / log /% HOSTNAME% /% PROGRAMNAME% .log"*. *? Incoming-logs & ~
````

![Alt-Text](images/log2.jpg)

Reinicie o serviço e verifique as portas rsyslog no Linux.

Quando fazemos qualquer tipo de alteração, devemos reiniciar o serviço executando uma das seguintes opções:

````
sudo service rsyslog restart
````

````
sudo systemctl restart rsyslog
````

Para verificar as portas usadas pelo rsyslog, executaremos o seguinte:

````
sudo netstat -tulpn | grep rsyslog
````

Como já indicamos, a porta utilizada será 514, devemos habilitá-la no firewall para uso com as seguintes linhas:

RedHat/CentOS:

````
firewall-cmd --permanent --add-port = 514/tcp firewall-cmd -reload
````

Debian:

````
ufw allow 514/tcp ufw allow 514/udp
````

No IPtables:

````
iptables -A INPUT -p tcp -m tcp --dport 514 -j ACCEPT iptables -A INPUT -p udp --dport 514 -j ACCEPT
````

![Alt-Text](images/log3.jpg)

Desta forma, configuramos o rsyslogs para receber logs de outros servidores e se tornar um centralizador de logs.

## Configurando o envio de logs dos servidores (client) para o centralizador

O servidor que irá enviar os logs também precisará possuir o rsyslogs instalado, caso já não tenha.

Debian:

````
sudo apt-get install Rsyslog
````

RedHat/CentOS:

````
sudo yum install Rsyslog
````

Para configurar o rsyslog para ser executado no modo cliente, devemos editar o arquivo de configuração no diretório “/etc/rsyslog.conf”.

Em alguns sistemas operacionais pode ser que o arquivo mude estando na diretório “/etc/rsyslog.d/”.

Podemos acessar usando o editor desejado:

````
sudo vi /etc/rsyslog.conf
````

Buscando até o campo “Rules”, que é onde ficam as regras de onde serão enviados os logs:

![Alt-Text](images/log4.jpg)

Iremos configurar para que todos os logs sejam enviados ao nosso centralizador, adicionando “*.*” e o IP do centralizador com “@” antes, sendo  que “@” será usado protocolo UDP e “@@” será usado protocolo TCP.

![Alt-Text](images/log5.jpg)

No exemplo da figura 5 utilizado IP 172.0.0.1, mas que deverá ser substituído pelo IP do servidor onde foi configurado o recebimento dos logs (centralizador).

Sendo assim finalizamos a configuração de recebimento e de envio de logs a um servidor remoto.

## Configurando o logrotate no centralizador

Agora falta somente configurar o logrotate no servidor que está recebendo os logs para que haja rotação de logs antigos.

Em servidores Linux normalmente o logrotate já vem instalado, mas caso não tenha basta usar o comando:

Debian:

````
sudo apt install logrotate
````

RedHat/CentOS

````
sudo yum install logrotate
````

Para alterar as configurações padrões de rotação devemos acessar “/etc/logrotate.conf.

![Alt-Text](images/log6.jpg)

- **weekly**: alternar semanalmente, as opções são: daily, weekly, monthly,yearly.
- **rotate**: os logs antigos são mantidos por 4 semanas nessa atual configuração default (4 rotações, dependendo de quando eles giram, mensalmente manteria 4 meses)
- **create**: após a rotação do log, cria um novo arquivo
- **dateext**: após a rotação adicionar data, por padrão, um número seria usado. (log.1, log2, log3).
- **compress**: vamos economizar espaço comprimindo os arquivos, o padrão é “tar.gz”.
- **include**: /etc/logrotate.d : esta é a parte mais importante, ela incluirá as rotações específicas da aplicação.

Específico: as configurações acima mostradas são pra rotação geral, ou seja de todos os logs, mas se por exemplo seja necessário fazer a rotação de apenas alguns específicos, é necessário criar um arquivo em “/etc/logrotate.d/arquivo”, com o conteúdo apontando para qual arquivo de log em específico será rotacionado, exemplo:

````
/var/log/exemplo.log {
rotate 52
weekly
create
compress
}
````

No exemplo acima os logs exemplo.log seriam rotacionados a cada 52 semanas (1 ano), criando um novo arquivo e comprimido.

**Testando a configuração**

Não precisaremos esperar o prazo de rotação definido para validar se a configuração está concluída, podemos forçar a rotação com o comando:

````
logrotate -v -f /etc/logrotate.d/exemplo
````

## Sobre a implementação feita:

Logrotate: Foi configurado a rotação de logs a cada 4 semanas que é o padrão, o servidor centralizador é o 172.26.13.141 “srv-log”.

E os clients que enviam os logs são:

- 172.30.0.76
- 172.30.0.45
- 172.30.0.48
- 172.30.0.11
- 172.30.0.36
- 172.26.14.2

O centralizador também foi adicionado a monitoração e já pode ser consultado no Grafana buscando por “srv-log”, sendo possível analisar os recursos de infraestrutura do host.

















